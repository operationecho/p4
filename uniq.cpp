#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include<map>
using std::map;
#include<vector>
using std::vector;
#include <iostream>
using std::cin;
using std::cout;
#include<set>
using std::set;
#include<algorithm>
using std::sort;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of uniq.  Supported options:\n\n"
"   -c,--count         prefix lines by their counts.\n"
"   -d,--repeated      only print duplicate lines.\n"
"   -u,--unique        only print lines that are unique.\n"
"   --help             show this message and exit.\n";

int main(int argc, char *argv[]) {
	// define long options
	static int showcount=0, dupsonly=0, uniqonly=0;
	static struct option long_opts[] = {
		{"count",         no_argument, 0, 'c'},
		{"repeated",      no_argument, 0, 'd'},
		{"unique",        no_argument, 0, 'u'},
		{"help",          no_argument, 0, 'h'},
		{0,0,0,0}
	};


	// Variable decralration for C
	map<string,int>count;
	string words;
  //Variable declaration for d
	vector<string>duplicates;
	//int dupcount;
	string temp;
	//Variable declarations for u
	vector<string>listOfWords;
	map<string,int> wordCount;

	// process options:

	char c;
	int opt_index = 0;

	while ((c = getopt_long(argc, argv, "cduh", long_opts, &opt_index)) != -1) {

		switch (c) {
			case 'c':
				showcount=1;
			break;

			case 'd':
				dupsonly = 1;

				break;

			case 'u':
				uniqonly = 1;

				break;

			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
/* TODO: write me... */
/*****************************SHOWCOUNT *************/
if(showcount==1)
{
		while(cin>>words)
			{
			++count[words];
			}

					for(map<string,int>::iterator i=count.begin();i!=count.end();i++)
					{
					cout<<(*i).second<<"\t"<<(*i).first<<"\n";
					}


}//end of showcount loop
/************************END OF SHOWCOUNT*****************/


/******************DUPLICATE ******************/
	if(dupsonly==1)
	{
	//vector<string>duplicates;



	vector<string>listOfDupWords;
	map<string,int>dupWordCount;
	while(cin>>words)
	{
		if(dupWordCount[words]==0)
			listOfDupWords.push_back(words);
						dupWordCount[words]++;
	}
		for(size_t i=0;i<listOfDupWords.size();i++)
		{
			if(dupWordCount[listOfDupWords[i]]!=1)
			cout<<listOfDupWords[i]<<"\n";
		}
	}



/*********************END OF DUPLICATE*****************/

	/**********************UNIQONLY********************/
	if(uniqonly==1)
	{
	vector<string>listOfWords;
	map<string,int>wordCount;
	while(cin>>words)
	{
		if(wordCount[words]==0)
			listOfWords.push_back(words);
						wordCount[words]++;
	}
		for(size_t i=0;i<listOfWords.size();i++)
		{
			if(wordCount[listOfWords[i]]==1)
			cout<<listOfWords[i]<<"\n";
		}
	}


	/****************END OF UNIQONLY********************/


	return 0;
}
